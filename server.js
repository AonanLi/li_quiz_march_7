// server.js

var express  = require('express');
var app      = express();
var server   = require( 'http' ).createServer( app );
var port     = process.env.PORT || 8080;
var passport = require('passport');
var flash    = require('connect-flash');
app.use(express.static(__dirname + '/public'));
var morgan       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');



var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/lab2');

app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.use( cookieParser()); 
app.use( bodyParser.json());
app.use( bodyParser.urlencoded({
	extended: true
}));


require('./config/passport')(passport); // pass passport for configuration

// set up our express application
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser()); // get information from html forms

app.set('view engine', 'ejs'); // set up ejs for templating

// required for passport
app.use(session({ secret: 'ilovescotchscotchyscotchscotch' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

// routes ======================================================================
require('./app/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport

// launch ======================================================================
app.listen(port);

console.log('Listening on port ' + port);